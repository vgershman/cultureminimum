package ru.datastack.culturalminimum

import android.app.Application
import io.realm.Realm

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }

    companion object Config {
        const val HOST = "http://appmin.datastack.ru"
    }
}