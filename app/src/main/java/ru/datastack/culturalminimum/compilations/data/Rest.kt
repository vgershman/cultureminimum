package ru.datastack.culturalminimum.compilations.data

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import ru.datastack.culturalminimum.App.Config.HOST

//rest

object Rest {

    private var service: RestApi? = null

    fun instance(): RestApi {
        val builder = OkHttpClient.Builder()
        if (service == null) {
            service = Retrofit.Builder()
                .baseUrl(HOST)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build().create(Rest.RestApi::class.java)
        }
        return service!!
    }


    interface RestApi {

        @GET("v2/Compilation")
        fun listCompilations(): Call<List<CompilationDto>>

        @GET("v2/Image/{id}")
        fun getImageMeta(@Path("id")id : String) : Call<ImageMetaDto>

    }
}
