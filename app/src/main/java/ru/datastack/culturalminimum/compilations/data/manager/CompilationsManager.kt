package ru.datastack.culturalminimum.compilations.data.manager

import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.datastack.culturalminimum.compilations.data.CompilationDto
import ru.datastack.culturalminimum.compilations.data.Repo
import ru.datastack.culturalminimum.compilations.data.Rest

//kind 'presenter' layer. Working with data and proceeding it to UI in any comfortable way.

object CompilationsManager {

    fun reloadCompilations(error: () -> Unit) {
        Rest.instance()
            .listCompilations().enqueue(object : Callback<List<CompilationDto>> {

            override fun onFailure(call: Call<List<CompilationDto>>, t: Throwable) {
                //todo real error check needed
                error()
            }

            override fun onResponse(
                call: Call<List<CompilationDto>>,
                response: Response<List<CompilationDto>>
            ) {
                //todo response check needed
                Repo.storeCompilations(response.body().orEmpty().toMutableList())
            }

        })
    }

    object CompilationLive : LiveData<List<CompilationDto>>() {

        override fun onActive() {
            super.onActive()
            reloadLocal()
        }

        fun reloadLocal() {
            postValue(Repo.readCompilations())
        }
    }

}