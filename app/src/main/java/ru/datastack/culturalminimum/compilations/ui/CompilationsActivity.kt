package ru.datastack.culturalminimum.compilations.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_compilations.*
import kotlinx.android.synthetic.main.item_compilation.view.*
import org.readium.r2.shared.Injectable
import org.readium.r2.streamer.parser.EpubParser
import org.readium.r2.streamer.parser.PubBox
import org.readium.r2.streamer.server.Server
import ru.datastack.culturalminimum.R
import ru.datastack.culturalminimum.compilations.data.CompilationDto
import ru.datastack.culturalminimum.compilations.data.manager.CompilationsManager
import ru.datastack.culturalminimum.compilations.data.manager.ImagesManager
import java.io.File
import java.io.IOException
import java.net.ServerSocket
import kotlin.random.Random

//views and events proceeded to manager and back.

class CompilationsActivity : AppCompatActivity() {

    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compilations)

        val s = ServerSocket(0)
        s.localPort
        s.close()

        localPort = s.localPort
        server = Server(localPort)

        R2DIRECTORY = if (true) {
            this.getExternalFilesDir(null)?.path + "/"
        } else {
            this.filesDir.path + "/"
        }

        preferences = getSharedPreferences("org.readium.r2.settings", Context.MODE_PRIVATE)//PreferenceManager.getDefaultSharedPreferences(this)
        rvCompilations.layoutManager = LinearLayoutManager(this)
        rvCompilations.adapter = CompilationsAdapter { onOpen(it) }
        CompilationsManager.CompilationLive.observe(this, Observer {
            srl.isRefreshing = false
            (rvCompilations.adapter as CompilationsAdapter).update(it)
        })
        srl.setOnRefreshListener {
            reload()
        }
        reload()
        srl.isRefreshing = true

    }


    private fun onOpen(it: CompilationDto) {
        UnknownHelper.goUrl(
            "http://appmin.datastack.ru/attachments/BFItem/attach/47773ed5-8749-45b5-a4c9-729cf67cb4e9.epub",
            this
        ) { onBook(it) }

    }

    lateinit var R2DIRECTORY: String

    override fun onDestroy() {
        super.onDestroy()
        stopServer()
    }

    override fun onStart() {
        super.onStart()
        startServer()
    }

    protected lateinit var server: Server
    private var localPort: Int = 0

    private fun onBook(book: Book) {


        val publicationPath = R2DIRECTORY + book.fileName
        val file = File(publicationPath)

        val parser = EpubParser()
        val pub = parser.parse(publicationPath)


        prepareToServe(pub, book.fileName, file.absolutePath)

        val intent = Intent(this, EpubActivity::class.java)
        intent.putExtra("publicationPath", publicationPath)
        intent.putExtra("publicationFileName", book.fileName)
        intent.putExtra("publication", pub?.publication)
        intent.putExtra("bookId", book.id)
        // intent.putExtra("cover", null)
        startActivity(intent)

    }

    protected fun prepareToServe(
        pub: PubBox?,
        fileName: String,
        absolutePath: String
    ) {
        if (pub == null) {
            return
        }
        val publication = pub.publication
        val container = pub.container


        preferences.edit()
            .putString("${publication.metadata.identifier}-publicationPort", localPort.toString())
            .apply()

        server.addEpub(
            publication,
            container,
            "/$fileName",
            applicationContext.filesDir.path + "/" + Injectable.Style.rawValue + "/UserProperties.json"
        )
    }

    private fun startServer() {
        if (!server.isAlive) {
            try {
                server.start()
            } catch (e: IOException) {
                // do nothing
            }
            if (server.isAlive) {

                // Add Resources from R2Navigator
                server.loadReadiumCSSResources(assets)
                server.loadR2ScriptResources(assets)
                server.loadR2FontResources(assets, applicationContext)

//                // Add your own resources here
//                server.loadCustomResource(assets.open("scripts/test.js"), "test.js")
//                server.loadCustomResource(assets.open("styles/test.css"), "test.css")
//                server.loadCustomFont(assets.open("fonts/test.otf"), applicationContext, "test.otf")

                //server.loadCustomResource(assets.open("Search/mark.js"), "mark.js", Injectable.Script)
                //server.loadCustomResource(assets.open("Search/search.js"), "search.js", Injectable.Script)
                //server.loadCustomResource(assets.open("Search/mark.css"), "mark.css", Injectable.Style)


            }
        }
    }

    private fun stopServer() {
        if (server.isAlive) {
            server.stop()
        }
    }



    private fun reload() {
        CompilationsManager.reloadCompilations { onReloadCompilationsError() }
    }

    private fun onReloadCompilationsError() {
        srl.isRefreshing = false
        Snackbar.make(
            rvCompilations,
            getString(R.string.err_network_possible),
            Snackbar.LENGTH_LONG
        ).show()
    }


    class CompilationsAdapter(var openCompilation: (comp: CompilationDto) -> Unit) :
        RecyclerView.Adapter<CompilationsAdapter.VH>() {

        private var data = arrayListOf<CompilationDto>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            VH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_compilation,
                    parent,
                    false
                )
            )

        override fun getItemCount() = data.size

        override fun onBindViewHolder(holder: VH, position: Int) {
            holder.bind(data[position])
        }

        fun update(compilations: List<CompilationDto>?) {
            data.clear()
            data.addAll(compilations.orEmpty())
            notifyDataSetChanged()
        }

        @ColorInt
        fun darkenColor(@ColorInt color: Int): Int {
            return Color.HSVToColor(FloatArray(3).apply {
                Color.colorToHSV(color, this)
                this[2] *= 0.65f
            })
        }

        inner class VH(view: View) : RecyclerView.ViewHolder(view) {

            fun bind(compilationDto: CompilationDto) {
                itemView.setOnClickListener { openCompilation(compilationDto) }
                itemView.tvTitle.text = compilationDto.title
                itemView.tvSubTitle.text = compilationDto.subtitle
                itemView.tvCounter.text =
                    "${Random.nextInt(
                        compilationDto.elements?.size ?: 0
                    )} из ${compilationDto.elements?.size ?: 0}"
                //like this until no localization needed
                if (compilationDto.coverArt != null) {
                    ImagesManager.loadImage(itemView.ivArt, compilationDto.coverArt!!)
                    itemView.ivArt.visibility = View.VISIBLE
                    itemView.cntArt.setBackgroundColor(Color.WHITE)
                    itemView.tvCounterBig.visibility = View.GONE
                    itemView.cntCaption.visibility = View.GONE
                } else {
                    val basicColor = Color.parseColor(compilationDto.color)
                    itemView.ivArt.visibility = View.GONE
                    itemView.tvCounterBig.visibility = View.VISIBLE
                    itemView.tvCounterBig.setTextColor(darkenColor(basicColor))
                    itemView.cntCaption.visibility = View.VISIBLE
                    itemView.tvCaption.text = compilationDto.caption.orEmpty()
                    itemView.tvCounterBig.text = (compilationDto.elements?.size ?: 0).toString()
                    itemView.cntArt.setBackgroundColor(basicColor)

                }
            }
        }
    }

}
