package ru.datastack.culturalminimum.compilations.data.manager

import android.widget.ImageView
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.datastack.culturalminimum.App.Config.HOST
import ru.datastack.culturalminimum.compilations.data.ImageMetaDto
import ru.datastack.culturalminimum.compilations.data.Repo
import ru.datastack.culturalminimum.compilations.data.Rest
import ru.datastack.culturalminimum.util.PositionedCropTransformation

//kind 'presenter' layer. Working with data and proceeding it to UI in any comfortable way.

object ImagesManager {

    fun loadImage(ivArt: ImageView, coverArt: String) {

        val localMeta = Repo.getImageMeta(coverArt)
        if (localMeta != null) {
            bindImage(ivArt, localMeta)
        }
        loadImageMeta(
            ivArt,
            coverArt,
            localMeta
        ) //load or reload
    }

    private fun loadImageMeta(ivArt: ImageView, coverArt: String, localMetaDto: ImageMetaDto?) {
        Rest.instance()
            .getImageMeta(coverArt).enqueue(object : Callback<ImageMetaDto> {

            override fun onFailure(call: Call<ImageMetaDto>, t: Throwable) {
                //todo error check
            }

            override fun onResponse(call: Call<ImageMetaDto>, response: Response<ImageMetaDto>) {
                //todo response check
                val imageMeta = response.body()
                if (imageMeta != null) {
                    Repo.storeImageMeta(imageMeta)
                    if (localMetaDto == null) {
                        bindImage(ivArt, imageMeta)
                    }
                }
            }

        })
    }

    //todo proper transformation needed
    fun bindImage(ivArt: ImageView, imageMetaDto: ImageMetaDto) {
        val url = imageMetaDto.attach?.preview?.url
        if (url?.contains("://") == true) {
            Glide.with(ivArt).load(url).optionalCenterCrop().into(ivArt)
        } else {
            Glide.with(ivArt).load(HOST + url).transform(
                PositionedCropTransformation(
                    0.5f,
                    0.25f
                )
            )
                .into(ivArt)
        }
    }

}