package ru.datastack.culturalminimum.compilations.data

import androidx.annotation.Keep
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

//all compilations POJOs adapted for Realm

@Keep
open class CompilationDto(
    @PrimaryKey var uuid: String? = null, var title: String? = null,
    var subtitle: String? = null,
    var color: String? = null,
    var caption: String? = null,
    var coverArt: String? = null,
    var elements: RealmList<ElementDto>? = null
) : RealmObject()

@Keep
open class ElementDto(var bfKey: String? = null) : RealmObject() //just thumb for realm

@Keep
open class ImageMetaDto(
    @PrimaryKey var uuid: String? = null, var _id: String? = null,
    var attach: ImageMetaAttach? = null
) : RealmObject()

@Keep
open class ImageMetaAttach(
    var original: ImageMeta? = null,
    var thumb: ImageMeta? = null,
    var preview: ImageMeta? = null
) : RealmObject()

@Keep
open class ImageMeta(var width: Int? = null, var height: Int? = null, var url: String? = null) : RealmObject()