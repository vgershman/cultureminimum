package ru.datastack.culturalminimum.compilations.data

import io.realm.ImportFlag
import io.realm.Realm
import ru.datastack.culturalminimum.compilations.data.manager.CompilationsManager

//database

object Repo {

    fun storeImageMeta(imageMeta: ImageMetaDto) {
        //todo async later
        Realm.getDefaultInstance().beginTransaction()
        Realm.getDefaultInstance().copyToRealmOrUpdate(imageMeta)
        Realm.getDefaultInstance().commitTransaction()
    }

    fun readCompilations() : List<CompilationDto>? = Realm.getDefaultInstance().where(CompilationDto::class.java).findAll() //todo async later

    fun getImageMeta(id: String) =
        Realm.getDefaultInstance().where(ImageMetaDto::class.java).equalTo("_id", id).findFirst() // todo async later


    fun storeCompilations(compilations: MutableList<CompilationDto>) {
        Realm.getDefaultInstance().executeTransactionAsync(Realm.Transaction {
            it.where(CompilationDto::class.java).findAll().deleteAllFromRealm()
            it.copyToRealmOrUpdate(compilations, ImportFlag.CHECK_SAME_VALUES_BEFORE_SET)
        }, Realm.Transaction.OnSuccess {
            CompilationsManager.CompilationLive.reloadLocal()
        })
    }

}