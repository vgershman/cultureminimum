/*
 * Module: r2-testapp-kotlin
 * Developers: Aferdita Muriqi, Mostapha Idoubihi, Paul Stoica
 *
 * Copyright (c) 2018. European Digital Reading Lab. All rights reserved.
 * Licensed to the Readium Foundation under one or more contributor license agreements.
 * Use of this source code is governed by a BSD-style license which is detailed in the
 * LICENSE file present in the project repository where this source code is maintained.
 */

package ru.datastack.culturalminimum.compilations.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.view.accessibility.AccessibilityManager
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_epub.*
import org.json.JSONArray
import org.readium.r2.navigator.epub.R2EpubActivity
import org.readium.r2.navigator.pager.R2EpubPageFragment
import org.readium.r2.navigator.pager.R2PagerAdapter
import org.readium.r2.shared.*
import org.readium.r2.shared.drm.DRM
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


/**
 * EpubActivity : Extension of the EpubActivity() from navigator
 *
 * That Activity manage everything related to the menu
 *      ( Table of content, User Settings, DRM, Bookmarks )
 *
 */
class EpubActivity : R2EpubActivity() , CoroutineScope {

    override var allowToggleActionBar: Boolean
        get() = false
        set(value) {}

    /**
     * Context of this scope.
     */
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

//    //UserSettings
//    lateinit var userSettings: UserSettings

    //Accessibility
    private var isExploreByTouchEnabled = false
    private var pageEnded = false

    // Provide access to the Bookmarks & Positions Databases
//    private lateinit var bookmarksDB: BookmarksDatabase
//    private lateinit var positionsDB: PositionsDatabase

    //  private lateinit var screenReader: R2ScreenReader

    protected var drm: DRM? = null
    protected var menuDrm: MenuItem? = null
    protected var menuToc: MenuItem? = null
    protected var menuBmk: MenuItem? = null
    protected var menuSearch: MenuItem? = null

    protected var menuScreenReader: MenuItem? = null

    private var bookId: Long = -1

    private var searchTerm = ""
    private lateinit var searchStorage: SharedPreferences
    //private lateinit var searchResultAdapter: SearchLocatorAdapter
    //private lateinit var searchResult: MutableList<SearchLocator>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //      bookmarksDB = BookmarksDatabase(this)
        //     positionsDB = PositionsDatabase(this)

        Handler().postDelayed({
            bookId = intent.getLongExtra("bookId", -1)
        }, 100)

        val appearancePref = preferences.getInt(APPEARANCE_REF, 0)
        val backgroundsColors = mutableListOf("#ffffff", "#faf4e8", "#000000")
        val textColors = mutableListOf("#000000", "#000000", "#ffffff")
        resourcePager.setBackgroundColor(Color.parseColor(backgroundsColors[appearancePref]))
        (resourcePager.focusedChild?.findViewById(org.readium.r2.navigator.R.id.book_title) as? TextView)?.setTextColor(
            Color.parseColor(textColors[appearancePref])
        )
        toggleActionBar()

        titleView.text = publication.metadata.title


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getBooleanExtra("returned", false)) {
                finish()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 2 && resultCode == Activity.RESULT_OK && data != null) {
                val locator = data.getSerializableExtra("locator") as Locator
                locator.locations?.fragment?.let { fragment ->

                    val fragments = JSONArray(fragment).getString(0).split(",").associate {
                        val (left, right) = it.split("=")
                        left to right.toInt()
                    }

                    val index = fragments.getValue("i").toInt()
                    val searchStorage =
                        getSharedPreferences("org.readium.r2.search", Context.MODE_PRIVATE)
                    Handler().postDelayed({
                        if (publication.metadata.rendition.layout == RenditionLayout.Reflowable) {
                            val currentFragent =
                                (resourcePager.adapter as R2PagerAdapter).getCurrentFragment() as R2EpubPageFragment
                            val resource = publication.readingOrder[resourcePager.currentItem]
                            val resourceHref = resource.href ?: ""
                            val resourceType = resource.typeLink ?: ""
                            val resourceTitle = resource.title ?: ""

                            currentFragent.webView.runJavaScript(
                                "markSearch('${searchStorage.getString(
                                    "term",
                                    null
                                )}', null, '$resourceHref', '$resourceType', '$resourceTitle', '$index')"
                            ) { result ->

                                // Timber.d("###### $result")

                            }
                        }
                    }, 1200)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        /*
         * If TalkBack or any touch exploration service is activated
         * we force scroll mode (and override user preferences)
         */
        val am = getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager
        isExploreByTouchEnabled = am.isTouchExplorationEnabled

        if (isExploreByTouchEnabled) {

            //Preset & preferences adapted
            publication.userSettingsUIPreset[ReadiumCSSName.ref(SCROLL_REF)] = true
            preferences.edit().putBoolean(SCROLL_REF, true).apply() //overriding user preferences

//            userSettings = UserSettings(preferences, this, publication.userSettingsUIPreset)
            //           userSettings.saveChanges()

//            Handler().postDelayed({
//                userSettings.resourcePager = resourcePager
//                userSettings.updateViewCSS(SCROLL_REF)
//            }, 500)
        } else {
            if (publication.cssStyle != ContentLayoutStyle.cjkv.name) {
                publication.userSettingsUIPreset.remove(ReadiumCSSName.ref(SCROLL_REF))
            }

//            userSettings = UserSettings(preferences, this, publication.userSettingsUIPreset)
            //           userSettings.resourcePager = resourcePager
        }


    }

    override fun toggleActionBar() {
        val am = getSystemService(ACCESSIBILITY_SERVICE) as AccessibilityManager
        isExploreByTouchEnabled = am.isTouchExplorationEnabled

        if (!isExploreByTouchEnabled && tts_overlay.visibility == View.INVISIBLE) {
            super.toggleActionBar()
        }

    }


    override fun onPageEnded(end: Boolean) {
        if (isExploreByTouchEnabled) {
            if (!pageEnded == end && end) {
                //toast("End of chapter")
            }

            pageEnded = end
        }
    }

}