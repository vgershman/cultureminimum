package ru.datastack.culturalminimum.compilations.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.github.kittinunf.fuel.Fuel
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.then
import nl.komponents.kovenant.ui.successUi
import org.joda.time.DateTime
import org.json.JSONObject
import org.readium.r2.opds.OPDS1Parser
import org.readium.r2.opds.OPDS2Parser
import org.readium.r2.shared.Publication
import org.readium.r2.shared.opds.ParseData
import org.readium.r2.shared.promise
import ru.datastack.culturalminimum.OPDSDownloader
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class Book(
    val fileName: String,
    val title: String,
    val author: String?,
    val fileUrl: String,
    var id: Long?,
    val coverLink: String?,
    val identifier: String,
    val cover: ByteArray?,
    val ext: Publication.EXTENSION,
    val creation: Long = DateTime().toDate().time
)

object UnknownHelper {


    private lateinit var opdsDownloader: OPDSDownloader

    private fun getDownloadURL(publication: Publication): URL? {
        var url: URL? = null
        val links = publication.links
        for (link in links) {
            val href = link.href
            if (href != null) {
                if (href.contains(".epub") || href.contains(".lcpl")) {
                    url = URL(href)
                    break
                }
            }
        }
        return url
    }

    fun authorName(publication: Publication): String {
        return publication.metadata.authors.firstOrNull()?.name?.let {
            return@let it
        } ?: run {
            return@run String()
        }
    }


    private fun getBitmapFromURL(src: String): Bitmap? {
        return try {
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            val bitmap = BitmapFactory.decodeStream(input)
            bitmap
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }


    private fun downloadData(url: String, onBookCreated: (book: Book) -> Unit) {

        //   val publication = parseData.publication ?: return


        opdsDownloader.publicationUrl(url).successUi { pair ->

            val publicationIdentifier = "123456"//publication.metadata.identifier
            val author = "Author"//authorName(publication)

            val book = Book(
                pair.second,
                "title",
                author,
                pair.first,
                null,
                "",
                publicationIdentifier,
                null,
                Publication.EXTENSION.EPUB
            )
            onBookCreated(book)


        }
    }

    private fun parseURL(url: URL): Promise<ParseData, Exception> {
        return Fuel.get(url.toString(), null).promise() then {
            val (_, _, result) = it
            if (isJson(result)) {
                OPDS2Parser.parse(result, url)
            } else {
                OPDS1Parser.parse(result, url)
            }
        }
    }

    private fun isJson(byteArray: ByteArray): Boolean {
        return try {
            JSONObject(String(byteArray))
            true
        } catch (e: Exception) {
            false
        }
    }

    fun goUrl(url: String, context: Context, callback: (book: Book) -> Unit) {
        opdsDownloader = OPDSDownloader(context)
        //val parseDataPromise = parseURL(URL(url))
        //parseDataPromise.successUi { parseData ->
        downloadData(url, callback)
        //}
        //parseDataPromise.failUi {
        //   Log.v("wtf", it.toString())
        // }
    }
}